﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki
{
    public class CommandsConfig
    {
        ButtonsState state;

        public CommandsConfig()
        {
            state = new ButtonsStateStopped();
        }

        public void savePlayList(EncapsulatedVariables variables)
        {
            state.savePlayList(variables);
        }

        public void nextTrack(EncapsulatedVariables variables)
        {
            state.nextTrack(variables);
        }

        public void previusTrack(EncapsulatedVariables variables)
        {
            state.previusTrack(variables);
        }

        public void stopMusic(EncapsulatedVariables variables)
        {
            state.stopMusic(variables);
            state = new ButtonsStateStopped();
        }

        public void playMusic(EncapsulatedVariables variables)
        {
            state = new ButtonsStatePlaying();
            state.playMusic(variables);
        }

        public void pauseMusic(EncapsulatedVariables variables)
        {
            state.pauseMusic(variables);
        }

        public void openFileList(EncapsulatedVariables variables)
        {
            state.openFileList(variables);
        }

        internal void loadPlayList(EncapsulatedVariables variables)
        {
            state.loadPlayList(variables);
        }
    }
}
