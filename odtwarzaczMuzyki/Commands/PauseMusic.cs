﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki
{
    public class PauseMusic : Command
    {
        public void execute(EncapsulatedVariables veri)
        {
            veri.MyPlayer.Pause();
        }
    }
}
