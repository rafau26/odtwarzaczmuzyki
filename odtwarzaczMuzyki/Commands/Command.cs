﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki
{
    public interface Command
    {
        void execute(EncapsulatedVariables veri);
    }
}
