﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace odtwarzaczMuzyki
{
    public class NextTrack : Command
    {
        public async void execute(EncapsulatedVariables veri)
        {
            //zatrzymianie odtwarzania bierzacego utowru
            veri.MyPlayer.Stop();
            //zwikeszenie ID biezacego utowru
         //   veri.IDBiezacyUtwor++;
            //jezeli id jest wieksze niz lista wczytanych utworow
            //to odtwarzany jest pierwszy element z listy
         //   if (veri.IDBiezacyUtwor >= veri.TrackCount)
         //   {
         //       veri.IDBiezacyUtwor = 0;
          //  }
            //odtworzenie nastepnego pliku
            //w ten sposob mozna podac stream do radia
            // tutaj rmf max
            //veri.MyPlayer.Source = new Uri("http://31.192.216.5:9002");

            veri.IDBiezacyUtwor++;
            if (veri.IDBiezacyUtwor >= veri.TrackCount)
               {
                  veri.IDBiezacyUtwor = 0;
             }
            var file = await StorageFile.GetFileFromPathAsync(PlayList.PlayList.Instance.SongList[veri.IDBiezacyUtwor].FilePath);
            //var file = await StorageFile.GetFileFromPathAsync(PlayList.PlayList.Instance.next().FilePath);
            var stream = await file.OpenAsync(FileAccessMode.Read);
            veri.MyPlayer.SetSource(stream, file.FileType); 
            veri.MyPlayer.Play();


            PlayList.PlayList.Instance.CurrentAlbum = PlayList.PlayList.Instance.SongList[veri.IDBiezacyUtwor].Album;
            PlayList.PlayList.Instance.CurrentArtist = PlayList.PlayList.Instance.SongList[veri.IDBiezacyUtwor].Artist;
            PlayList.PlayList.Instance.CurrentTitle = PlayList.PlayList.Instance.SongList[veri.IDBiezacyUtwor].Title;
            //PlayList.PlayList.Instance.c
            
        }
    }
}
