﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki
{
    public class PlayMusic : Command
    {
        public void execute(EncapsulatedVariables veri)
        {
            //jezeli nie ma wczytanej jeszcze listy plikow
            //to nalezy ja wczytac
            if (veri.TrackCount == 0)
            {
                Commander com = new Commander();
                Command OPF = new OpenFileList();
                com.execute(OPF, veri);
            }
            else
            {
                veri.MyPlayer.Play();
            }
        }
    }
}
