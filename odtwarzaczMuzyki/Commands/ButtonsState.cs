﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki
{
    public abstract class ButtonsState
    {
        public void savePlayList(EncapsulatedVariables variables)
        {
            Command command = new SavePlayList();
            Invoker invoker = new Invoker();
            invoker.SetCommand(command);
            invoker.ExecuteCommand(variables);
        }

        public abstract void nextTrack(EncapsulatedVariables variables);

        public abstract void previusTrack(EncapsulatedVariables variables);

        public abstract void stopMusic(EncapsulatedVariables variables);

        public abstract void playMusic(EncapsulatedVariables variables);

        public abstract void pauseMusic(EncapsulatedVariables variables);

        public void openFileList(EncapsulatedVariables variables)
        {
            Command command = new OpenFileList();
            Invoker invoker = new Invoker();
            invoker.SetCommand(command);
            invoker.ExecuteCommand(variables);
        }

        public void loadPlayList(EncapsulatedVariables variables)
        {
            Command command = new LoadPlayList();
            Invoker invoker = new Invoker();
            invoker.SetCommand(command);
            invoker.ExecuteCommand(variables);
        }
    }
}
