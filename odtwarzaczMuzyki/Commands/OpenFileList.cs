﻿using odtwarzaczMuzyki.PlayList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;

namespace odtwarzaczMuzyki
{
    public class OpenFileList : Command
    {

        public async void execute(EncapsulatedVariables veri)
        {
            try
            {
                //otwieranie pliku
                var FileOpen = new FileOpenPicker();
                FileOpen.SuggestedStartLocation = PickerLocationId.Desktop;
                FileOpen.FileTypeFilter.Add(".MP3");
                FileOpen.FileTypeFilter.Add(".WMA");
                //var File = await FileOpen.PickSingleFileAsync();
                IReadOnlyList<StorageFile> File;
                File = await FileOpen.PickMultipleFilesAsync();
                //veri.MP.TBplayList.Text = "";
                veri.Lista = 0;
                veri.TrackCount = 0;
                Song sn = new Song(File[0].Path);
                veri.TrackCount = 0;

                bool flag = true;
                foreach (StorageFile f in File)
                {
                    Song song = new Song(f.Path.ToString());

                    if (flag)
                    {
                        PlayList.PlayList.Instance.CurrentAlbum = song.Album;
                        PlayList.PlayList.Instance.CurrentArtist = song.Artist;
                        PlayList.PlayList.Instance.CurrentTitle = song.Title;
                        flag = false;
                    }

                    PlayList.PlayList.Instance.SongList.Add(song);


                    //ListViewItem item = new ListViewItem();
                    //veri.Tracki.Add(f.Path.ToString());
                    //veri.MP.TBplayList.Text += f.Name.ToString();
                    //TBplayList.Text += f.Path.ToString();
                    veri.TrackCount++;

                }

                
                
                File = null;
                
                //otwieranie pierwszego pliku z listy wybranych plikow
                //var Stream = await veri.File[0].OpenAsync((FileAccessMode.Read));
                //var Stream = await File.OpenAsync(FileAccessMode.Read);
                //veri.MyPlayer.SetSource(Stream, veri.File[0].ContentType);

                var file = await StorageFile.GetFileFromPathAsync(PlayList.PlayList.Instance.SongList[0].FilePath);
                var stream = await file.OpenAsync(FileAccessMode.Read);
                veri.MyPlayer.SetSource(stream, file.FileType);
                veri.IDBiezacyUtwor = 0;

                
                //PlayList.PlayList.Instance.Plp.checkVersionPlayList();
                

                PlayList.PlayList.Instance.CurrentAlbum = PlayList.PlayList.Instance.SongList[0].Album;
                PlayList.PlayList.Instance.CurrentArtist = PlayList.PlayList.Instance.SongList[0].Artist;
                PlayList.PlayList.Instance.CurrentTitle = PlayList.PlayList.Instance.SongList[0].Title;

               // PlayListProxy plp = new PlayListProxy();
               // PlayList.PlayList.Instance.setUniquePlayListId("0");
               // plp.checkVersionPlayList();

            }
            catch (IOException ex)
            {
                //MessageDialog msg = new MessageDialog("Wybierz jakiś plik");
                MessageDialog msg = new MessageDialog(ex.Message);
                msg.ShowAsync();
            }
        }
    }
}
