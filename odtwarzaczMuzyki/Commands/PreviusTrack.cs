﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace odtwarzaczMuzyki
{
    public class PreviusTrack : Command
    {
        public async void execute(EncapsulatedVariables veri)
        {
            //zatrzymanie odtwarzania
            veri.MyPlayer.Stop();
            //zmniejszenie ID
            veri.IDBiezacyUtwor--;
            //tutaj możba by jeszcze zrobic ze jak id <0 to przrzuca na element ostani, czyli o najwiekszym id
            if (veri.IDBiezacyUtwor < 0)
            {
                //jezeli id mniejsze od 0 tzn ze jestesmy na 1 utworze, czyli nie ma gdzie sie cofnac
                veri.IDBiezacyUtwor++;
            }
            var file = await StorageFile.GetFileFromPathAsync(PlayList.PlayList.Instance.SongList[veri.IDBiezacyUtwor].FilePath);
            //var file = await StorageFile.GetFileFromPathAsync(PlayList.PlayList.Instance.prev().FilePath);
            var stream = await file.OpenAsync(FileAccessMode.Read);
            veri.MyPlayer.SetSource(stream, file.FileType);
            veri.MyPlayer.Play();


            PlayList.PlayList.Instance.CurrentAlbum = PlayList.PlayList.Instance.SongList[veri.IDBiezacyUtwor].Album;
            PlayList.PlayList.Instance.CurrentArtist = PlayList.PlayList.Instance.SongList[veri.IDBiezacyUtwor].Artist;
            PlayList.PlayList.Instance.CurrentTitle = PlayList.PlayList.Instance.SongList[veri.IDBiezacyUtwor].Title;
        }
    }
}
