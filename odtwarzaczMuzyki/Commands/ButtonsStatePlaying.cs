﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki
{
    class ButtonsStatePlaying : ButtonsState
    {

        public override void nextTrack(EncapsulatedVariables veri)
        {
            Command command = new NextTrack();
            Invoker invoker = new Invoker();
            invoker.SetCommand(command);
            invoker.ExecuteCommand(veri);
        }

        public override void previusTrack(EncapsulatedVariables veri)
        {
            Command command = new PreviusTrack();
            Invoker invoker = new Invoker();
            invoker.SetCommand(command);
            invoker.ExecuteCommand(veri);
        }

        public override void stopMusic(EncapsulatedVariables veri)
        {
            Command command = new StopMusic();
            Invoker invoker = new Invoker();
            invoker.SetCommand(command);
            invoker.ExecuteCommand(veri);
        }

        public override void playMusic(EncapsulatedVariables veri)
        {
            Command command = new PlayMusic();
            Invoker invoker = new Invoker();
            invoker.SetCommand(command);
            invoker.ExecuteCommand(veri);
        }

        public override void pauseMusic(EncapsulatedVariables veri)
        {
            Command command = new PauseMusic();
            Invoker invoker = new Invoker();
            invoker.SetCommand(command);
            invoker.ExecuteCommand(veri);
        }


    }
}
