﻿using odtwarzaczMuzyki.PlaylistBuilder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Provider;
using Windows.UI.Popups;

namespace odtwarzaczMuzyki
{
    public class SavePlayList : Command
    {
        public async void execute(EncapsulatedVariables veri)
        {

            Config conf = new Config(PlayList.PlayList.Instance.SongList, "test");
            PlaylistCreator creator = new PlaylistCreator(conf);
            StringBuilder playlist = creator.getPlaylist();
            FileSavePicker savePicker = new FileSavePicker();
            savePicker.FileTypeChoices.Add("Playlist", new List<string> { ".txt" });
            savePicker.SuggestedStartLocation = PickerLocationId.MusicLibrary;
            savePicker.SuggestedFileName = "New playlist";


            StorageFile file = await savePicker.PickSaveFileAsync();
            if (file != null)
            {
                // Prevent updates to the remote version of the file until we finish making changes and call CompleteUpdatesAsync.
                CachedFileManager.DeferUpdates(file);
                await FileIO.WriteTextAsync(file, playlist.ToString());
                FileUpdateStatus status = await CachedFileManager.CompleteUpdatesAsync(file);
                if (status == FileUpdateStatus.Complete)
                {
                    MessageDialog msgDialog = new MessageDialog("File " + file.Name + " was saved.", "Save result");
                    //OutputTextBlock.Text = "File " + file.Name + " was saved.";
                }
                else
                {
                    MessageDialog msgDialog = new MessageDialog("File " + file.Name + " couldn't be saved.", "Save result");
                    //OutputTextBlock.Text = "File " + file.Name + " couldn't be saved.";
                }
            }
            else
            {
                //OutputTextBlock.Text = "Operation cancelled.";
            }
            
            /*
            Boolean error = false;
            try
            {
                //domyslnie folder zapisu: moja muzyka
                StorageFolder folder = KnownFolders.MusicLibrary;
                StorageFile sampleFile = await folder.CreateFileAsync("sample.txt", CreationCollisionOption.ReplaceExisting);
                /*
                 * playlist
                 * nazwa
                 * cursor {-2 - default, -1 - nowa nie grana play lista, n - jakas pozycja
                 * ilosc trackow
                 * tracki....
                 * 
                */
            /*    
            string header = "Name: " + veri.Name + "#\n" + veri.Cursor.ToString() + "#\n" + veri.TrackCount.ToString() + "#\n";
                string trackiSource = "";
                foreach (string s in veri.Tracki)
                {
                    trackiSource += "$" + s + "#\n";
                }
                var buffer = Windows.Security.Cryptography.CryptographicBuffer.ConvertStringToBinary(header + trackiSource, Windows.Security.Cryptography.BinaryStringEncoding.Utf8);
                await Windows.Storage.FileIO.WriteBufferAsync(sampleFile, buffer);
                var messageDial = new MessageDialog("Zapisano playliste", "Zapis pliku");
                await messageDial.ShowAsync();
            }
            catch (IOException a)
            {
                error = true;
            }
            if (error)
            {
                var messageDialog = new MessageDialog("Problem z zapisaem play listy");
                messageDialog.Commands.Add(new UICommand("Jeszcze raz", new UICommandInvokedHandler(this.CommandInvokedHandler)));
                messageDialog.Commands.Add(new UICommand("Zamknij", new UICommandInvokedHandler(this.CommandInvokedHandler)));
                messageDialog.DefaultCommandIndex = 0;
                messageDialog.CancelCommandIndex = 1;
                await messageDialog.ShowAsync();
            }*/
        }
        private void CommandInvokedHandler(IUICommand command)
        {
            // wyjatek do obsłuzenia (problem z zapisem pliku)
        }
    }
}
