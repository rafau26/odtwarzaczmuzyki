﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;


namespace odtwarzaczMuzyki.Observer
{
    public interface ObserverInterface
    {
        void update(double currentSeconds, double totalSeconds, double volume, Grid layoutRoot, double value);
        void update(double currentSeconds, double totalSeconds, double volume);
    }
}
