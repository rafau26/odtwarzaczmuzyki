﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Windows.UI.Xaml.Controls;





namespace odtwarzaczMuzyki.Observer
{
    class volumeObserver : ObserverInterface
    {
        private MediaElement MyPlayer;
        //private double currentSeconds;
        //private double totalSeconds;
        //private double volume;
        Gradients gradientGetter;
        public volumeObserver(MediaElement mPlayer)
        {
            this.MyPlayer = mPlayer;
            this.gradientGetter = new Gradients();     
        }


        public void update(double currentSeconds, double totalSeconds, double volume, Grid layoutRoot, double val)
        {
            MyPlayer.Volume = volume / 100;
            layoutRoot.Background = gradientGetter.getBackgroundGradient(currentSeconds, totalSeconds, volume);

        }
        public void update(double currentSeconds, double totalSeconds, double volume)
        {
            MyPlayer.Volume = volume / 100;
        }
    }
}
