﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace odtwarzaczMuzyki.Observer
{
    public class SliderObservered :ObservedInterface
    {
        private List<ObserverInterface> observers;
        private double currentSeconds;
        private double totalSeconds;
        private double volume;
        private double value;
        private Grid layoutRoot;
        public SliderObservered()
        {
            this.observers = new List<ObserverInterface>();
        }

        public void addObserver(ObserverInterface o)
        {
            observers.Add(o);
        }
        public void removeObserver(ObserverInterface o)
        {
            observers.Remove(o);
        }
        public void changeValue (double curSec, double totalSec, double vol, Grid layoutRoot, double value)
        {
            this.totalSeconds = totalSec;
            this.volume = vol;
            this.currentSeconds = curSec;
            this.layoutRoot = layoutRoot;
            this.value = value;
            notify();
        }
        public void notify()
        {
            foreach (ObserverInterface oi in observers)
            {
                oi.update(this.currentSeconds, this.totalSeconds , this.volume,  this.layoutRoot, this.value);
                //oi.update(this.currentSeconds, this.totalSeconds, this.volume);
            }
        }
        public void initialNotify()
        {
        }
    }
}