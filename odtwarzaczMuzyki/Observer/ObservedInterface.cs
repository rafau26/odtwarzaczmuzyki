﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki.Observer
{
    public interface ObservedInterface
    {
        void addObserver(ObserverInterface o);
        void removeObserver(ObserverInterface o);
        void notify();

        void initialNotify();
    }
}
