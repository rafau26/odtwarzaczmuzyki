﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Windows.UI.Xaml.Controls;


namespace odtwarzaczMuzyki.Observer
{
    //obserwowany
    public class VolumeObserved : ObservedInterface
    {
        private List<ObserverInterface> observers;
        private double currentSeconds;
        private double totalSeconds;
        private double volume;
        private double value;
        private Grid layoutRoot;

        public VolumeObserved()
        {
            this.observers = new List<ObserverInterface>();
            this.value = 0;
            
        }
        public void addObserver(ObserverInterface o)
        {
            observers.Add(o);
        }
        public void removeObserver(ObserverInterface o)
        {
            observers.Remove(o);
        }
        public void changeValue (double curSec, double totalSec, double vol, Grid layoutRoot )
        {
            this.totalSeconds = totalSec;
            this.volume = vol;
            this.currentSeconds = curSec;
            this.layoutRoot = layoutRoot;
            notify();
        }
        public void changeValue(double curSec, double totalSec, double vol)
        {
            this.totalSeconds = totalSec;
            this.volume = vol;
            this.currentSeconds = curSec;
            initialNotify();
        }

        public void initialNotify()
        {
            foreach (ObserverInterface oi in observers)
            {
                oi.update(this.currentSeconds, this.totalSeconds , this.volume);
            }
        }

        public void notify()
        {
            foreach (ObserverInterface oi in observers)
            {
                oi.update(this.currentSeconds, this.totalSeconds , this.volume, this.layoutRoot, this.value);
            }
        }
        
    }
}
