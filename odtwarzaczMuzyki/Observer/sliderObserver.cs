﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace odtwarzaczMuzyki.Observer
{
    class SliderObserver : ObserverInterface
    {
        private MediaElement MyPlayer;
        Gradients gradientGetter;
        public SliderObserver(MediaElement mPlayer)
        {
            this.MyPlayer = mPlayer;
            gradientGetter = new Gradients();
        }
        public void update(double currentSeconds, double totalSeconds, double volume, Grid layoutRoot, double value)
        {
            if (value != -1)
            {
                MyPlayer.Position = TimeSpan.FromSeconds(value);
            }
            layoutRoot.Background = gradientGetter.getBackgroundGradient(MyPlayer.Position.TotalSeconds, MyPlayer.NaturalDuration.TimeSpan.TotalSeconds, volume);

        }
        public void update(double currentSeconds, double totalSeconds, double volume)
        {
        }
    }
}
