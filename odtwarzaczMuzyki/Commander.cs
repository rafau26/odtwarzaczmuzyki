﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki
{
    public class Commander
    {
        public void execute(Command command, EncapsulatedVariables veri)
        {
            command.execute(veri);
        }
    }
}
