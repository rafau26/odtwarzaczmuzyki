﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml.Media;
using System.Text;
using odtwarzaczMuzyki.PlaylistBuilder;
using Windows.Storage.Pickers;
using Windows.Storage.Provider;
using odtwarzaczMuzyki.PlayList;
using Windows.UI;
using Windows.Foundation;
using Windows.UI.Xaml.Shapes;
using System.Collections.ObjectModel;
using System.Linq;
using odtwarzaczMuzyki.Observer;
using System.Runtime.CompilerServices;
using System.ComponentModel;


namespace odtwarzaczMuzyki
{
    //klasa ktora przechowywuje wszystkie zmienne programu
    public class EncapsulatedVariables
    {

        // private IReadOnlyList<StorageFile> _File;
        private int _lista;
        private int _IDBiezacyUtwor;
        private int _trackCount = 0; //pierdoły: ilosc plikow 
        //private List<Song> _trackList;
        private MainPage _MP;
        private MediaElement _MyPlayer;
        private VolumeObserved _volObservered; //obiekt obserwowany
        private volumeObserver volObserver; // obserwator
        private SliderObserver sliderObserver; // observator
        private SliderObservered _sliderObserved; //obiekt obserwowany


        /* public IReadOnlyList<StorageFile> File
         {
             get { return _File; }
             set { _File = value; }
         }*/

        public int Lista
        {
            get { return _lista; }
            set { _lista = value; }
        }

        public int IDBiezacyUtwor
        {
            get { return _IDBiezacyUtwor; }
            set { _IDBiezacyUtwor = value; }
        }

        public int TrackCount
        {
            get { return _trackCount; }
            set { _trackCount = value; }
        }

        //public List<Song> TrackList
        //{
        //    get { return _trackList; }
        //    set { _trackList = value; }
        //}

        public MainPage MP
        {
            get { return _MP; }
            set { _MP = value; }
        }

        public MediaElement MyPlayer
        {
            get { return _MyPlayer; }
            set { _MyPlayer = value; }
        }

        public VolumeObserved volumeObservered
        {
            get { return _volObservered; }
        }

        public SliderObservered sliderObservered
        {
            get { return _sliderObserved; }
        }


        public EncapsulatedVariables(MainPage obj, MediaElement MyPlayer)
        {
            _lista = 0;
            _IDBiezacyUtwor = 0;
            //_tracki = new List<string>();
            //_trackList = new List<Song>();
            this._MP = obj;
            if (MyPlayer == null)
            {
                this.MyPlayer = new MediaElement();
                this.MyPlayer.Volume = 100;
            }
            else
            {
                this.MyPlayer = MyPlayer;
            }


            //utworzenie obserwatorow
            _volObservered = new VolumeObserved();
            volObserver = new volumeObserver(MyPlayer);
            _volObservered.addObserver(volObserver); //dodanie obseratora
            _sliderObserved = new SliderObservered();
            sliderObserver = new SliderObserver(MyPlayer);
            _sliderObserved.addObserver(sliderObserver);
        }

    }

    public partial class MainPage : Page
    {

        public EncapsulatedVariables variables;
        public CommandsConfig commandsConfig;
        public DispatcherTimer _timer;
        Gradients gradientGetter;

        public MainPage()
        {
            this.InitializeComponent();
            commandsConfig = new CommandsConfig();
            _timer = new DispatcherTimer();
            volSlider.Value = 100;
            this.variables = new EncapsulatedVariables(this, MyPlayer);
            PlayList.PlayList.Instance.setVeri(variables);

        }

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            timelineSlider.ValueChanged += timelineSlider_ValueChanged;
            PointerEventHandler pointerpressedhandler = new PointerEventHandler(timelineSlider_PointerEntered);
            timelineSlider.AddHandler(Control.PointerPressedEvent, pointerpressedhandler, true);

            PointerEventHandler pointerreleasedhandler = new PointerEventHandler(timelineSlider_PointerCaptureLost);
            timelineSlider.AddHandler(Control.PointerCaptureLostEvent, pointerreleasedhandler, true);
            MyPlayer.Volume = 100;
            //CreateARectangleWithLGBrush();
            PopulateList();
            gradientGetter = new Gradients();
            playMode_Normal.Background = new SolidColorBrush(Colors.Blue);
        }

        //-------------------------------------------------------------------------------------
        //--Buttons----------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------

        private void openFile_Click(object sender, RoutedEventArgs e)
        {
            addedF = true;
            commandsConfig.openFileList(variables);
            //PlayListProxy plp = new PlayListProxy();
            //PlayList.PlayList.Instance.setUniquePlayListId("0");
            //plp.checkVersionPlayList();
            //PopulateList();
        }

        private void play_Click(object sender, RoutedEventArgs e)
        {
            commandsConfig.playMusic(variables);

        }
        private void pause_Click(object sender, RoutedEventArgs e)
        {
            commandsConfig.pauseMusic(variables);
        }
        private void stop_Click(object sender, RoutedEventArgs e)
        {
            commandsConfig.stopMusic(variables);
        }
        private void nextTrack_Click(object sender, RoutedEventArgs e)
        {
            commandsConfig.nextTrack(variables);
        }
        private void proviousTrack_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            commandsConfig.previusTrack(variables);
        }
        private void savePlayList_Click(object sender, RoutedEventArgs e)
        {
            commandsConfig.savePlayList(variables);
        }
        private void readPlayList_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            commandsConfig.loadPlayList(variables);
        }

        //--Buttons-playmode-------------------------------------------------------------------

        private void playMode_Normal_Click(object sender, RoutedEventArgs e)
        {
            PlayList.PlayList.Instance.setVeri(variables);

            playMode_Normal.Background = new SolidColorBrush(Colors.Blue);
           
            playMode_RepeatAll.Background = new SolidColorBrush(Colors.Transparent);
            playMode_RepeatOne.Background = new SolidColorBrush(Colors.Transparent);
            playMode_Shuffle.Background = new SolidColorBrush(Colors.Transparent);

            PlayList.PlayList.Instance.setOrderSongIterator();
        }

        private void playMode_RepeatAll_Click(object sender, RoutedEventArgs e)
        {
            playMode_RepeatAll.Background = new SolidColorBrush(Colors.Blue);

            playMode_Normal.Background = new SolidColorBrush(Colors.Transparent);
            playMode_RepeatOne.Background = new SolidColorBrush(Colors.Transparent);
            playMode_Shuffle.Background = new SolidColorBrush(Colors.Transparent);


            PlayList.PlayList.Instance.setRepeatAllSongIterator(variables);
        }

        private void playMode_RepeatOne_Click(object sender, RoutedEventArgs e)
        {
            playMode_RepeatOne.Background = new SolidColorBrush(Colors.Blue);

            playMode_Normal.Background = new SolidColorBrush(Colors.Transparent);
            playMode_RepeatAll.Background = new SolidColorBrush(Colors.Transparent);
            playMode_Shuffle.Background = new SolidColorBrush(Colors.Transparent);

            PlayList.PlayList.Instance.setRepeatOneSongIterator();
        }

        private void playMode_Shuffle_Click(object sender, RoutedEventArgs e)
        {
            playMode_Shuffle.Background = new SolidColorBrush(Colors.Blue);

            playMode_RepeatOne.Background = new SolidColorBrush(Colors.Transparent);
            playMode_Normal.Background = new SolidColorBrush(Colors.Transparent);
            playMode_RepeatAll.Background = new SolidColorBrush(Colors.Transparent);

            PlayList.PlayList.Instance.setShuffleSongIterator();
        }


        //-------------------------------------------------------------------------------------
        //--MyPlayer---------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------

        bool tmpp = true;
        private void MyPlayer_MediaOpened(object sender, RoutedEventArgs e)
        {
            double absvalue = (int)Math.Round(
                this.variables.MyPlayer.NaturalDuration.TimeSpan.TotalSeconds,
                MidpointRounding.AwayFromZero);

            timelineSlider.Maximum = absvalue;
            timelineSlider.StepFrequency = 1;
            trackLength.Text = MyPlayer.NaturalDuration.ToString().Substring(0, 8);
            //if (tmpp)
            //{
            //    PopulateList();
            //     tmpp = false;
            // }

            setDetails();
            SetupTimer();
        }

        private bool flagg = false;

        bool tmp = true;

        private void setDetails()
        {
            //Song sng = PlayList.PlayList.Instance.getCurrentSong(this.variables);
            tb_album.Text = "Album: " + PlayList.PlayList.Instance.CurrentAlbum;
            tb_artist.Text = "Artist: " + PlayList.PlayList.Instance.CurrentArtist;
            tb_title.Text = "Title: " + PlayList.PlayList.Instance.CurrentTitle;

            tb_number.Text = "Number of songs:  " + PlayList.PlayList.Instance.getNumberOfElementsInPlayList();
            tb_len.Text = "Playlist lenght:  " + PlayList.PlayList.Instance.getLongPlayList();

            //MessageDialog dlg = new MessageDialog(tb_len.Text.ToString(), tb_number.Text.ToString());
            //dlg.ShowAsync();
            //PlayListProxy plp = new PlayListProxy();

            if (tmp)
            {
                //PlayList.PlayList.Instance.setUniquePlayListId("0");
                tmp = false;
            }
            else
            {
                PlayList.PlayList.Instance.setUniquePlayListId(PlayList.PlayList.Instance.CurrentTitle);
            }
            PlayList.PlayList.Instance.Plp.checkVersionPlayList();
            flagg = false;

            //plp.checkVersionPlayList();
        }

        private void MyPlayer_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            if (this.variables.MyPlayer.CurrentState == MediaElementState.Playing)
            {
                if (_sliderpressed)
                {
                    _timer.Stop();
                }
                else
                {
                    _timer.Start();
                }
            }

            if (this.variables.MyPlayer.CurrentState == MediaElementState.Paused)
            {
                _timer.Stop();
            }

            if (this.variables.MyPlayer.CurrentState == MediaElementState.Stopped)
            {
                _timer.Stop();
                timelineSlider.Value = 0;
            }
        }

        private void MyPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            StopTimer();
            timelineSlider.Value = 0.0;
            commandsConfig.nextTrack(variables);
        }

        private void MyPlayer_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            string hr = GetHresultFromErrorMessage(e);
        }

        //-------------------------------------------------------------------------------------
        //--ProgressSlider---------------------------------------------------------------------
        //-------------------------------------------------------------------------------------

        private bool _sliderpressed = false;

        void timelineSlider_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            _sliderpressed = true;
        }

        void timelineSlider_PointerCaptureLost(object sender, PointerRoutedEventArgs e)
        {
            this.variables.MyPlayer.Position = TimeSpan.FromSeconds(timelineSlider.Value);
            _sliderpressed = false;
        }

        void timelineSlider_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            if (!_sliderpressed)
            {
                variables.sliderObservered.changeValue(MyPlayer.Position.TotalSeconds, MyPlayer.NaturalDuration.TimeSpan.TotalSeconds, volSlider.Value, LayoutRoot, e.NewValue);
                TrackTime.Text = this.variables.MyPlayer.Position.ToString();
                //this.variables.MyPlayer.Position = TimeSpan.FromSeconds(e.NewValue);
                //LayoutRoot.Background = gradientGetter.getBackgroundGradient(MyPlayer.Position.TotalSeconds, MyPlayer.NaturalDuration.TimeSpan.TotalSeconds, volSlider.Value);
            }
        }

        private bool addedF = true;
        private void SetupTimer()
        {
            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromSeconds(timelineSlider.StepFrequency);
            StartTimer();
            if (addedF)
            {
                PopulateList();
                addedF = false;
            }
        }


        private void _timer_Tick(object sender, object e)
        {
            if (!_sliderpressed)
            {
                TrackTime.Text = this.variables.MyPlayer.Position.ToString();
                timelineSlider.Value = MyPlayer.Position.TotalSeconds;
                variables.sliderObservered.changeValue(MyPlayer.Position.TotalSeconds, MyPlayer.NaturalDuration.TimeSpan.TotalSeconds, volSlider.Value, LayoutRoot, -1);
                //LayoutRoot.Background = gradientGetter.getBackgroundGradient(MyPlayer.Position.TotalSeconds, MyPlayer.NaturalDuration.TimeSpan.TotalSeconds, volSlider.Value);
                //setDetails();
            }
        }

        private bool lolFlag = true;

        private void StartTimer()
        {
            _timer.Tick += _timer_Tick;
            _timer.Start();
            if (lolFlag)
            {
                //PopulateList();
                lolFlag = false;
            }
        }

        private void StopTimer()
        {
            _timer.Stop();
            lolFlag = true;
            tmpp = true;
            _timer.Tick -= _timer_Tick;
        }

        //-------------------------------------------------------------------------------------
        //--Other------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------

        private string GetHresultFromErrorMessage(ExceptionRoutedEventArgs e)
        {
            String hr = String.Empty;
            String token = "HRESULT - ";
            const int hrLength = 10;     // eg "0xFFFFFFFF"

            int tokenPos = e.ErrorMessage.IndexOf(token, StringComparison.Ordinal);
            if (tokenPos != -1)
            {
                hr = e.ErrorMessage.Substring(tokenPos + token.Length, hrLength);
            }

            return hr;
        }

        private void volChange(object sender, RangeBaseValueChangedEventArgs e)
        {
            try
            {
                //LayoutRoot.Background = gradientGetter.getBackgroundGradient(MyPlayer.Position.TotalSeconds, MyPlayer.NaturalDuration.TimeSpan.TotalSeconds, volSlider.Value);
                variables.volumeObservered.changeValue(MyPlayer.Position.TotalSeconds, MyPlayer.NaturalDuration.TimeSpan.TotalSeconds, volSlider.Value, LayoutRoot);

            }
            catch
            {
                //NIE MOGE SOBIE Z TYM PORADZIC ALE OGOLNIE DZIALA
                //#####!@#$%^&()*&^%$$#@#$%^&*(*&^%$#@#$%^&*()_(*&^%$#@#$%^&*()*&^%$#@@#$%^&*
                // variables.volumeObservered.changeValue(MyPlayer.Position.TotalSeconds, MyPlayer.NaturalDuration.TimeSpan.TotalSeconds, 100);
            }
            TBVolume.Text = volSlider.Value.ToString();

        }

        // ---------------------------------------------------------------

        private void PopulateList()
        {
           
           
            var result = from act in PlayList.PlayList.Instance.SongList group act by act.Artist into grp orderby grp.Key select grp;
            cvsActivities.Source = result;
        }

        private async void StackPanel_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var btn = sender as TextBlock;

            Song sng = PlayList.PlayList.Instance.findAndPlaySong(btn.Text);

            var file = await StorageFile.GetFileFromPathAsync(sng.FilePath);
            var stream = await file.OpenAsync(FileAccessMode.Read);
            this.variables.MyPlayer.SetSource(stream, file.FileType);

            tb_album.Text = "Album: " + PlayList.PlayList.Instance.CurrentAlbum;
            tb_artist.Text = "Artist: " + PlayList.PlayList.Instance.CurrentArtist;
            tb_title.Text = "Title: " + PlayList.PlayList.Instance.CurrentTitle;

            // MessageDialog dlg = new MessageDialog(btn.Text.ToString());
            //dlg.ShowAsync();
        }
         

    }

}
