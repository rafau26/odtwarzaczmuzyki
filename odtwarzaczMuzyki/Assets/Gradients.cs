﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml.Media;

namespace odtwarzaczMuzyki
{
    class Gradients
    {
        public LinearGradientBrush getBackgroundGradient(double currentSeconds, double totalSeconds, double volume)
        {
            double x = currentSeconds * (Math.PI / totalSeconds);

            double y = Math.Sin(x);
            double value = 0.8 - (y * 0.7);

            LinearGradientBrush gradient = new LinearGradientBrush();

            gradient.StartPoint = new Point(0.5, 0);
            gradient.EndPoint = new Point(0.5, 1);

            byte toWhite = (byte)(140 - (140 * volume / 100));

            GradientStop gradientBottom = new GradientStop();
            gradientBottom.Color = Color.FromArgb(255, toWhite, toWhite, 140);
            gradientBottom.Offset = 1.0;
            gradient.GradientStops.Add(gradientBottom);

            GradientStop gradientTop = new GradientStop();
            gradientTop.Color = Color.FromArgb(255, 17, 17, 17);
            try { 
            gradientTop.Offset = value;
            }
            catch
            {
                gradientTop.Offset = 0.8;
            }
            gradient.GradientStops.Add(gradientTop);

            return gradient;
        }
    }
}
