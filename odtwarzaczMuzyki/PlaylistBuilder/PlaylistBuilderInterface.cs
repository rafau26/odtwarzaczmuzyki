﻿using System;
using System.Text;

namespace odtwarzaczMuzyki.PlaylistBuilder
{
    /*
        ##PlaylistTitle
        ##JÓNIKUŁEHashId
        ##OverallTime
        ##HowMany
        ##Cursor_num
        #in_song_#PATH#SongTittle#Artist#Length#Rating
     */
    abstract class PlaylistBuilderInterface
    {
        public static String LINE_BEGINNING = "##";
        public static String WORD_SEPARATOR = "#";
        public static String NOW_MANY_SONGS = "HowManySongs";
        public static String UNIQUE_ID = "UniquePlaylistID";
        public static String OVERALL_TIME = "OverallTimeOfThePlaylist";
        public static String TITLE = "PlaylistTitle";

        public StringBuilder strBuilder;

        public PlaylistBuilderInterface(String playlistName)
        {
            strBuilder = new StringBuilder(LINE_BEGINNING)
                .Append(TITLE).Append(WORD_SEPARATOR)
                .Append(playlistName).AppendLine();
            generateUniqueId();
        }

        //public abstract void addTitle(String title);
        public abstract void addHowManySongs(int count);
        public abstract void addOverallTime(String overallTime);
        public abstract void generateUniqueId();
        public abstract void addSong(Song song);

        public StringBuilder getResult()
        {
            return strBuilder;
        }
    }
}
