﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki.PlaylistBuilder
{
    class PlaylistCreator
    {
        private List<Song> list;
        private Config config;
        private PlaylistBuilderInterface plBuilder;

        public PlaylistCreator(Config config)
        {
            this.list = config.List;
            this.plBuilder = config.Builder;
            this.config = config;
        }

        private void build()
        {
            //this.plBuilder.addOverallTime
            this.plBuilder.addHowManySongs(list.Count);
            foreach (Song song in list)
            {
                this.plBuilder.addSong(song);
            }
        }

        public StringBuilder getPlaylist()
        {
            build();
            return this.plBuilder.getResult();
        }
    }
}
