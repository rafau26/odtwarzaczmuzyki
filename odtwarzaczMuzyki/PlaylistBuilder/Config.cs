﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki.PlaylistBuilder
{
    class Config
    {
        private List<Song> list;
        private PlaylistBuilderInterface builder;


        public Config(List<Song> list, String playlistName)
        {
            this.list = list;
            /* 
             * defaultowy builder - jeśli chcielibyśmy np dodać jakiś inny 
             * format pliku do zapisu to wystarczy go zaimplementować 
             * i wywołać po konstruktorze metodę setBuilder();
             */
            this.builder = new OurPlaylistBuilder(playlistName);
        }

        internal PlaylistBuilderInterface Builder
        {
            get { return builder; }
            set { builder = value; }
        }

        public List<Song> List
        {
            // setter zablokowany, ustawiane tylko konstruktorem
            private set {}
            get { return list; }
        }
    }
}
