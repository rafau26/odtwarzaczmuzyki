﻿using System;

namespace odtwarzaczMuzyki.PlaylistBuilder
{
    class OurPlaylistBuilder : PlaylistBuilderInterface
    {
        //strBuilder.Append(LINE_BEGINNING).Append().AppendLine();
        //strBuilder.Append(LINE_BEGINNING).Append().Append(WORD_SEPARATOR).Append().AppendLine();
        //#in_song_#PATH#SongTittle#Artist#Length#Rating

        private int songId;

        /*
         * Tytuł i UniqueId są na starcie
         * a flaga jest po to aby nie dało się ustawić overallTime albo howManySongs 
         * pomiędzy piosenkami. jak już dodamy chociaż jedną piosenkę
         * to metody te przestają działać.
         * my o tym wiemy, ale koncepcyjnie coś takiego powinno się tu znaleźć
         */
        private bool firstSongAddedFlag;
        private bool idUniqueIdGeneratedFlag;

        public OurPlaylistBuilder(String playlistName)
            : base(playlistName)
        {
            songId = 0;
            firstSongAddedFlag = false;
            idUniqueIdGeneratedFlag = false;
        }

        // tego prawdopodobnie nie będzie
        /*public override void addTitle(string title)
       {
           
           strBuilder.Append(LINE_BEGINNING).Append(TITLE)
               .Append(WORD_SEPARATOR).Append(title);
             
       }*/

        public override void addHowManySongs(int amount)
        {
            if (firstSongAddedFlag) return;
            strBuilder.Append(LINE_BEGINNING).Append(NOW_MANY_SONGS)
                .Append(WORD_SEPARATOR).Append(amount).AppendLine();
        }

        // Chwilowo Strinh, ewentualnie takiś DateTime czy coś tego typu i cast do stringa
        public override void addOverallTime(string overallTime)
        {
            if (firstSongAddedFlag) return;
            strBuilder.Append(LINE_BEGINNING).Append(OVERALL_TIME)
                .Append(WORD_SEPARATOR).Append(overallTime).AppendLine();
        }

        public override void generateUniqueId()
        {
            if (idUniqueIdGeneratedFlag) return;
            Guid g = Guid.NewGuid();
            string uniqueID = Convert.ToBase64String(g.ToByteArray());
            uniqueID = uniqueID.Replace("=", "");
            uniqueID = uniqueID.Replace("+", "");
            strBuilder.Append(LINE_BEGINNING).Append(UNIQUE_ID)
                .Append(WORD_SEPARATOR).Append(uniqueID).AppendLine();
            idUniqueIdGeneratedFlag = true;
        }

        public override void addSong(Song song)
        {
            firstSongAddedFlag = true;
            strBuilder.Append(LINE_BEGINNING).Append(songId++)
                .Append(WORD_SEPARATOR).Append(song.FilePath)
                .Append(WORD_SEPARATOR).Append(song.Artist)
                .Append(WORD_SEPARATOR).Append(song.Title)
                .Append(WORD_SEPARATOR).Append(song.TrackLength)
                .Append(WORD_SEPARATOR).Append(song.Rating)
                .AppendLine();
        }
    }
}
