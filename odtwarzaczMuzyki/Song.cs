﻿using System;
using Windows.Storage;
using Windows.Storage.FileProperties;

namespace odtwarzaczMuzyki
{
    public class Song
    {
        private String artistName;
        private String name;
        private String songTitle;
        private String albumTitle;
        private String trackLength;
        private String trackBitrate;
        private float rating;
        private String filePath;
        private int seconds;

        public Song(string path)
        {
            this.filePath = path;
            this.SongConstructor();
        }
        //this.variables.MyPlayer.Position
        //konstruktor nie moze byc async z tad ta funkcja
        private async void SongConstructor()
        {
            StorageFile file = await StorageFile.GetFileFromPathAsync(this.filePath);
            MusicProperties musicProperties = await file.Properties.GetMusicPropertiesAsync();
            if (musicProperties.Artist != "")
            {
                this.albumTitle = musicProperties.Album;
                this.artistName = musicProperties.Artist;
                this.songTitle = musicProperties.Title;
                this.name = this.artistName + " - " + this.songTitle;
            }
            else
            {
                // TODO - trzeba jeszcze przyciąć ścieżkę, tak żeby splita robić tylko z jej końcówki
                // w sensie po ostatnim '/'
                String[] tmp = new String[3];
                tmp = file.Name.Split('-');
                this.songTitle = tmp[1].Trim();
                this.artistName = tmp[0].Trim();
            }
            this.trackBitrate = musicProperties.Bitrate.ToString();
            this.trackLength = musicProperties.Duration.ToString().Substring(0, 8);
            this.rating = musicProperties.Rating;
            this.seconds = countSeconds(musicProperties);
        }

        private int countSeconds(MusicProperties musicProperties)
        {
            int tmpSeconds = 0;
            tmpSeconds += musicProperties.Duration.Days * 24 * 60 * 60;
            tmpSeconds += musicProperties.Duration.Hours * 60 * 60;
            tmpSeconds += musicProperties.Duration.Minutes * 60;
            tmpSeconds += musicProperties.Duration.Seconds;
            return tmpSeconds;
        }


        public String Artist
        {
            get { return artistName; }
            set { artistName = value; }
        }

        public String Title
        {
            get { return songTitle; }
            set { songTitle = value; }
        }

        public String Album
        {
            get { return albumTitle; }
            set { albumTitle = value; }
        }

        public String TrackLength
        {
            get { return trackLength; }
            set { trackLength = value; }
        }

        public String TrackInfo
        {
            get { return trackBitrate; }
            set { trackBitrate = value; }
        }

        public float Rating
        {
            get { return rating; }
            set { rating = value; }
        }

        public String FilePath
        {
            get { return filePath; }
            set { filePath = value; }
        }

        public int Seconds
        {
            get { return seconds; }
        }
        public String Name
        {
            get { return name; }
            set { name = value; }
        }
    }
}
