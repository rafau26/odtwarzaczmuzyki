﻿using odtwarzaczMuzyki.Iterators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace odtwarzaczMuzyki.PlayList
{
    class PlayListProxy
    {
        protected int howManyElements = 0;
        protected String howLongIsPlayList = "";
        protected String lastUniqueIdPlaylist = "";

        public PlayListProxy() {
            checkVersionPlayList();  
        }

        public int getNumberOfElementsInPlayList() {
            return howManyElements;
        }

        public String getLongOfPlayList() {
            return howLongIsPlayList;
        }

        public void checkVersionPlayList()
        {
            if (getUniqueIdActualPlayList() != getUniqueIdFromLastPlayList())
            {
                VisitorHowManyElements visitatorHowManyElements = new VisitorHowManyElements();
                VisitorHowLongIsPlaylist visitatorHowLongIsPlaylist = new VisitorHowLongIsPlaylist();
                PlayList.Instance.accept(visitatorHowManyElements);
                PlayList.Instance.accept(visitatorHowLongIsPlaylist);
                howManyElements = visitatorHowManyElements.getResult();
                howLongIsPlayList = visitatorHowLongIsPlaylist.getResoult();
            }
            else 
            { 
            
            }
        }

        public String getUniqueIdFromLastPlayList() {
            String id = PlayList.Instance.getLastUniquePlayListId();
            return id;    
        }
         
        public String getUniqueIdActualPlayList() {
            String id = PlayList.Instance.getUniquePlayListId();
            return id;
        }

        public List<Song> getOrderSongIterator() {
            IteratorInterface orderIterator = PlayList.Instance.getOrderSongIterator();
            List<Song> songs = new List<Song>();
            while (orderIterator.hasNext()) {
                songs.Add(orderIterator.next());
            }
            return songs;
        }

        public List<Song> getRepeatAllSongIterator()
        {
            IteratorInterface repeatAllIterator = PlayList.Instance.getRepeatAllSongIterator();
            List<Song> songs = new List<Song>();
            while (repeatAllIterator.hasNext())
            {
                songs.Add(repeatAllIterator.next());
            }
            return songs;
        }

        public List<Song> getRepeatOneSongIterator()
        {
            IteratorInterface repeatOneIterator = PlayList.Instance.getRepeatOneSongIterator();
            List<Song> songs = new List<Song>();
            while (repeatOneIterator.hasNext())
            {
                songs.Add(repeatOneIterator.next());
            }
            return songs;
        }

        public List<Song> getShuffleSongIterator()
        {
            IteratorInterface shuffleIterator = PlayList.Instance.getShuffleSongIterator();
            List<Song> songs = new List<Song>();
            while (shuffleIterator.hasNext())
            {
                songs.Add(shuffleIterator.next());
            }
            return songs;
        }
    }
}