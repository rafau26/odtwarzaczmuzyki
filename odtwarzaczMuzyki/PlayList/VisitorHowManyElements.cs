﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki.PlayList
{
    class VisitorHowManyElements : VisitatorInterface
    {
        protected int size = 0;

        public void visit(Song song)
        {
            size++;
        }

        public int getResult() {
            return size;
        }
    }
}
