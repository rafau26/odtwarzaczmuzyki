﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki.PlayList
{
    class VisitorHowLongIsPlaylist : VisitatorInterface
    {
        protected int trackLong = 0;
        public void visit(Song song)
        {
            trackLong += song.Seconds;
        }

        public String getResoult()
        {

            TimeSpan t = TimeSpan.FromSeconds(trackLong);

            string answer = string.Format("{0:D2}:{1:D2}:{2:D2}",
                            t.Hours,
                            t.Minutes,
                            t.Seconds);

            //int seconds = trackLong % 60;

            //int minute = (trackLong - seconds) / 60;
            //int tmp = minute % 60;
            //int hour = (minute - tmp) / 60;
            //minute = tmp;

            //String resoult = hour.ToString() + " : " + minute.ToString() + " : " + seconds.ToString();
            return answer;
        }
    }
}
