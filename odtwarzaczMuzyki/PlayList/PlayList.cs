﻿using odtwarzaczMuzyki.Iterators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Popups;


namespace odtwarzaczMuzyki.PlayList
{
    class PlayList : VisitableInterface, IteratorInterface
    {
        private List<Song> songList = new List<Song>();
        private EncapsulatedVariables veri;

        private static volatile PlayList instance;
        public int actualPlaingTrack;
        private static object syncRoot = new Object();
        public String playListId = "0";
        public String lastPlayListId = "0";
        private IteratorInterface currentIterator;
        private PlayListProxy plp;// = new PlayListProxy(); 

        public PlayListProxy Plp
        {
            get { return plp; }
            set { plp = value; }
        }

        private string currentPath;

        public string CurrentPath
        {
            get { return currentPath; }
            set { currentPath = value; }
        }

        public List<Song> SongList
        {
            get { return songList; }
            set { songList = value; }
        }

        public Song getCurrentSong(EncapsulatedVariables variables)
        { 
            Song sng = new Song(CurrentPath);
            return sng;
        }

        private PlayList()
        {
            this.actualPlaingTrack = 0;
            setOrderSongIterator();
            setPlayListId();
 

            //plp = new PlayListProxy(); 
        }

        public static PlayList Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new PlayList();
                    }
                }
                return instance;
            }
        }

        public String getUniquePlayListId() 
        {
            return playListId;        
        }

        public String setUniquePlayListId(String id) {
            setLastUniquePlayListId(playListId);
            return this.playListId = id;
        }

        public String getLastUniquePlayListId()
        {
            return lastPlayListId;
        }

        public String setLastUniquePlayListId(String id)
        {
            return this.lastPlayListId = id;
        }

        public String setPlayListId() {
            playListId = LoadPlayList.getActualIdList;
            return playListId;
        }

        public String getLongPlayList (){
            if (plp == null)
            {
                plp = new PlayListProxy();
                plp.checkVersionPlayList();
            }
            return plp.getLongOfPlayList();
        }

        public int getNumberOfElementsInPlayList() {
            if (plp == null)
            {
                plp = new PlayListProxy();
                plp.checkVersionPlayList();
            }
            return plp.getNumberOfElementsInPlayList();
        }

        public IteratorInterface getOrderSongIterator()
        {
            return new OrderIterator(songList);
        }

        public IteratorInterface getRepeatAllSongIterator()
        {
            return new RepeatAllIterator(songList);
        }

        public IteratorInterface getShuffleSongIterator()
        {
            return new ShuffleIterator(songList);
        }

        public IteratorInterface getRepeatOneSongIterator()
        {
            return new RepeatOneIterator(songList, actualPlaingTrack);
        }

        public void accept(VisitatorInterface visitatorInterface)
        {
            foreach (Song song in songList)
            {
                visitatorInterface.visit(song);
            }
        }

        //-------------------------------------------------------------------------

        private string currentTitle;
        private string currentArtist;
        private string currentAlbum;

        public string CurrentTitle
        {
            get { return currentTitle; }
            set { currentTitle = value; }
        }

        public string CurrentArtist
        {
            get { return currentArtist; }
            set { currentArtist = value; }
        }

        public string CurrentAlbum
        {
            get { return currentAlbum; }
            set { currentAlbum = value; }
        }

        public void setOrderSongIterator()
        {
            currentIterator = new OrderIterator(songList);
        }

        public void setRepeatAllSongIterator()
        {
            currentIterator = new RepeatAllIterator(songList);
        }

        public void setShuffleSongIterator()
        {
            currentIterator = new ShuffleIterator(songList);
        }

        public void setRepeatOneSongIterator()
        {
            currentIterator = new RepeatOneIterator(songList, actualPlaingTrack);
        }

        public bool hasNext()
        {
            return currentIterator.hasNext();
        }

        public Song next()
        {
            return currentIterator.next();
        }


        public bool hasPrev()
        {
            return currentIterator.hasPrev();
        }
        public Song prev()
        {
            return currentIterator.prev();
        }

        public Song findAndPlaySong(string name)
        {
            Song sng = songList[0];
            foreach (Song song in songList)
            {
                if (song.Name.Equals(name))
                {
                    sng = song;
                }
            }

            currentAlbum = sng.Album;
            currentArtist = sng.Artist;
            CurrentTitle = sng.Title;

            return sng;
        }
    }
}
