﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki.Iterators
{
    class RepeatOneIterator : IteratorInterface
    {
        private List<Song> listSong;
        private int repeatTrackNumber = 0;

        public RepeatOneIterator (List<Song> listSong, int repeatTrackNumber) {
            this.listSong = listSong;
            this.repeatTrackNumber = repeatTrackNumber;
        }

        public bool hasNext()
        {
            return true; 
        }

        public Song next()
        {
            return listSong[repeatTrackNumber];
        }


        public bool hasPrev()
        {
            return true;
        }
        public Song prev()
        {
            return listSong[repeatTrackNumber];
        }
    }
}
