﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki.Iterators
{
    class ShuffleIterator : IteratorInterface
    {
        private List<Song> listSong;
        private int counter = 0;

        public ShuffleIterator (List<Song> listSong) {
            this.listSong = listSong;
            shuffle(listSong);
        }

        public void shuffle(List<Song> songs) {
            Random rng = new Random();
            int lenghtSongList = songs.Count;
            while (lenghtSongList > 1) {
                int p = rng.Next(lenghtSongList);
                int k = rng.Next(lenghtSongList);
                Song tmp = songs[p];
                songs[p] = songs[k];
                songs[k] = tmp;
                lenghtSongList--;
            }
        }

        public bool hasNext()
        {
            return listSong.Count <= counter ? false : true;
        }

        public Song next()
        {
            return listSong[counter++];
        }


        public bool hasPrev()
        {
            return true;
        }
        public Song prev()
        {
            return listSong[counter];
        }
    }
}
