﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace odtwarzaczMuzyki.Iterators
{
    class OrderIterator : IteratorInterface
    {
        private List<Song> listSong;
        private int counter = 0;


        public OrderIterator(List<Song> listSong)
        {
            this.listSong = listSong;

        }

        public bool hasNext() 
        {
            return listSong.Count == counter +1 ? false : true;
        }

        public int  getCounter()
        {
            return this.counter;
        }

        public Song next()
        {
            if (this.hasNext())
            {

                return listSong[++counter];
            }
            else
            {
                return listSong[counter];
            }
        }

        public bool hasPrev()
        {
            if (counter <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Song prev()
        {
            if (this.hasPrev())
            {
                return listSong[--counter];
            }
            else
            {
                return listSong[counter];
            }
        }
    }
}
