﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki.Iterators
{
    interface IteratorInterface
    {
        Boolean hasNext();
        Song next();
        Boolean hasPrev();
        Song prev();
    }
}
