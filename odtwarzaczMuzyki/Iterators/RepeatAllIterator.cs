﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odtwarzaczMuzyki.Iterators
{
    class RepeatAllIterator : IteratorInterface
    {
        private List<Song> listSong;
        private int counter = 0;
        EncapsulatedVariables veri;

        public RepeatAllIterator (List<Song> listSong, EncapsulatedVariables verrri) {
            this.veri = verrri;
            this.listSong = listSong;
        }


        public bool hasNext()
        {
            return true;
        } 

        public Song next()
        {
            if (++counter == listSong.Count) 
            {
                counter = 0;
                return listSong[counter];
            }
            else
            {
                return listSong[counter];
            }
        }


        public bool hasPrev()
        {
            return true;
        }
        public Song prev()
        {
            if (--counter < 0)
            {
                counter = listSong.Count - 1;
            }
            return listSong[counter];
        }

    }
}
